(defpackage :clml.pca.examples.pca-bench
  (:nicknames :pca.examples.pca-bench :pca-bench)
  (:use :cl
        :clml.hjs.meta
        ;:clml.hjs.matrix
        ;:clml.hjs.vector
        clml.hjs.read-data
        ;:clml.statistics
        ;:clml.hjs.vars
        :clml.pca
        )
)
